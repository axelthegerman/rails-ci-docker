# rails-ci-docker

https://hub.docker.com/r/axelthegerman/rails-ci

Ruby image for running/testing Ruby on Rails applications in CI.

`docker pull axelthegerman/rails-ci`

**Based on official ruby image**

Installs:
- firefox (ESR)
- postgresql-client
- nodejs and yarn

## Build and Push to Docker Hub

`$ make build` to build and tag as latest version or `$ make build VERSION=0.0.0` to tag as specific version.

Same for the `$ make push` command to push image to Docker Hub.
