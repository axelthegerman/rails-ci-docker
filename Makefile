.PHONY: build push

VERSION := $(VERSION)
VERSION_DIR := ${shell echo $(VERSION) | cut -c 1-3}

build:
	docker build -t axelthegerman/rails-ci:$(VERSION) $(VERSION_DIR)

push:
	docker push axelthegerman/rails-ci:$(VERSION)
